---
title: Installation
weight: 10
---
## Getting SHAR

SHAR is available as:
- A [Docker container](/installation/docker)
- A Kubernetes Helm Chart
- Buildable source code
