---
title: Docker 
weight: -20
---
## Docker container

### Prerequisites
The pre-requisite for SHAR is a running NATS instance or cluster.  An image can be found in the NATS [container registry](https://hub.docker.com/_/nats/).

### SHAR
The latest SHAR docker container is available in the project's [container registry](https://gitlab.com/shar-workflow/shar/container_registry/3061620?orderBy=NAME&sort=desc).

#### Parameters
The following environment variables are used when starting a SHAR server.

| Environment Variable | Description                          | default               |
|----------------------|--------------------------------------|-----------------------|
| NATS_URL             | the URL of the NATS server.          | NATS://127.0.0.1:4222 |
| SHAR_LOG_LEVEL       | the logging level (debug,warn,error) | debug                 |
