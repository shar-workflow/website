---
title: What is SHAR
geekdocNav: true
geekdocAlign: left
geekdocAnchor: false
---
SHAR is a way of running workflows at scale.
It can do this by harnessing the power of
distributed messaging to transition between workflow activities.

With SHAR you can:
- Execute business focused applications.
- Run distributed workflows that may span many machines or data-centres.

## BPMN workflow definitions
{{< bpmn url=simple-workflow.bpmn height=auto width=400 >}}

SHAR workflows can be defined in a BPMN diagram, either by a business analyst or a software developer.
The BPMN is then parsed by the SHAR client, and can then be executed either manually, triggered by an event, or at specific times.

Designing software this way, means that the diagram is the process.
This in turn results in self-documenting software.

## Tasks

The workflow may describe tasks that should be started.
These tasks may be software processes (Service Tasks), or user actions (User Tasks).
### Service tasks
Service tasks are discrete pieces of software which take inputs from the workflow, and return outputs back to the workflow.
### User tasks
User tasks describe long-running tasks that are assigned to one or more users.
These may be listed, and completed.
Their results get passed back to the workflow.