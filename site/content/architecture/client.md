---
title: Client
weight: 10
---
The SHAR client library performs the following roles:
- Provides a BPMN parser which converts BPMN to a SHAR state machine.
- Provides a programmatic interface to the SHAR API.
- Facilitates Service Task and Workflow Message registration.
- Listens for registered Service Task and Workflow Messages.
- Decodes and encodes workflow variables.

### API calls
The following diagram shows the message flow between SHAR Client and the Engine.

{{< mermaid class="text-center" >}}
sequenceDiagram
autonumber
    participant Client 
    participant NATS
    participant SHAR API
    participant Engine
    Client--)NATS: WORKFLOW.Api.LaunchWorkflow(request)
    activate Client
    NATS--)SHAR API: WORKFLOW.Api.LaunchWorkflow(request)
    activate SHAR API
    SHAR API->>Engine: launchWorkflow(*model.LaunchWorkflowRequest)
    SHAR API--)NATS: WORKFLOW.Api.LaunchWorkflow(response)
    deactivate SHAR API
    NATS--)Client: WORKFLOW.Api.LaunchWorkflow(response)
    deactivate Client
{{< /mermaid >}}

{{< hint type=note title=Request-Response >}}
In reality the request and response routing are transparent to the SHAR client, as they are handled by the NATS client and appear as a blocking call similar to GRPC.
{{< /hint >}}
### BPMN Parser
The BPMN Parser converts a BPMN file into a SHAR state machine model.
{{< hint type=note title="BPMN Files" >}}
Neither the SHAR API nor the SHAR Engine understand anything about BPMN.
The client is the only point of contact with the BPMN file content.  
{{< /hint >}}
Currently the client supports parsing Camunda Modeler 8 BPMN files created by the [Camunda Modeler](https://camunda.com/download/modeler/).
Most of the functionality from the modeler is supported including running sub-workflows and input and output transformation for variables.
{{< hint type=warning title="Expression Dialect" >}}
The syntax for all business expressions and scripts in SHAR is [Expr](https://github.com/antonmedv/expr).
The Camunda Modeler uses another expression language, and may therefore incorrectly flag expressions as invalid.
This will be fixed with the release of the SHAR modeler.
{{< /hint >}}
{{< hint type=caution title="Custom Types" >}}
All types used in variables passed to workflows must be primitive go types eg. string, int, []int, map[string]string.
Although the workflow engine is capable of operating with user defined structs, the serializer and deserializer will not understand how to handle them.
{{< /hint >}}