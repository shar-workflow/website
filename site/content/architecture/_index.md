---
title: Architecture
weight: 100
---

SHAR server uses [NATS Message Pull](https://docs.nats.io/nats-concepts/jetstream/consumers) combined with a [Stateless Decoupled Monolith](https://codeopinion.com/message-driven-architecture-to-decouple-a-monolith/) pattern to achieve linear scaling.

Each instance pulls a number of concurrent messages from NATS, performs tasks based on the messages and sends messages back to NATS for further processing.

The advantages of this are:
- SHAR server instances are unaware of their participation in a cluster.
- No configuration is needed to cluster SHAR servers.
- The topology is driven from NATS configuration.
- Any instance can handle any message.
- Failed processing is retried automatically.
- Servers only pull as many messages as they can handle.
- Removing or adding servers provides immediate performance alterations.

### Composition
SHAR is split into 3 components:
- The [Client](/architecture/client) which wraps API calls to the server, can send workflow messages, registers tasks, and runs tasks.
- The [Engine](/architecture/engine) which handles workflow processing, storage and orchestration.
- The Server which thinly wraps the engine, and provides [API](/architecture/api) access, and lifecycle management.