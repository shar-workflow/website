---
title: Api
weight: 20
---
 
SHAR's API is served using [NATS Request-Reply](https://docs.nats.io/nats-concepts/core-nats/reqreply), and each API method has its own NATS [queue group](https://docs.nats.io/nats-concepts/core-nats/queue).
This ensures that only one instance of SHAR services a single API call.

The wire format is [protobuf](https://developers.google.com/protocol-buffers/docs/overview).  This was chosen as it provides fast serialize / deserialize, and is familiar to cloud developers.

When an error occurs during an API call, an error response is generated with the magic prefix "ERR_".
This informs the client not to expect to decode a protobuf object, and instead decode an error code and message.

Minimal processing takes place in the API as it behaves as a gateway to the [SHAR engine](/architecture/engine).
