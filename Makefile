default: pages
clean:
	rm -rf site/public/*
	rm -rf site/resources/*
configure:
	cd site && \
	rm -rf themes/hugo-geekdoc && \
	mkdir -p themes/hugo-geekdoc/ && \
	curl -L https://github.com/thegeeklab/hugo-geekdoc/releases/latest/download/hugo-geekdoc.tar.gz | tar -xz -C themes/hugo-geekdoc/ --strip-components=1
pages: clean configure
	cd site && hugo
	rm -rf public
	cp -r site/public .